from django.urls import path
from . import views

app_name = 'bank_app'

urlpatterns = [
        path('', views.index, name='index'),
        path('frontpage_customer/', views.frontpage_customer, name='frontpage_customer'),
        path('account_details/<int:pk>/', views.account_details, name='account_details'),
        path('transaction_details/<int:transaction>/', views.transaction_details, name='transaction_details'),
        path('transfer_options/', views.transfer_options, name='transfer_options'),
        path('make_transfer/', views.make_transfer, name='make_transfer'),
        path('make_external_transfer/', views.make_external_transfer, name='make_external_transfer'),
        path('apply_loan/', views.apply_loan, name='apply_loan'),

        path('frontpage_staff/', views.frontpage_staff, name='frontpage_staff'),
        path('staff_search_partial/', views.staff_search_partial, name='staff_search_partial'),
        path('staff_customer_details/<int:pk>/', views.staff_customer_details, name='staff_customer_details'),
        path('staff_account_list_partial/<int:pk>/', views.staff_account_list_partial, name='staff_account_list_partial'),
        path('staff_account_details/<int:pk>/', views.staff_account_details, name='staff_account_details'),
        path('staff_new_account_partial/<int:user>/', views.staff_new_account_partial, name='staff_new_account_partial'),
        path('staff_new_customer/', views.staff_new_customer, name='staff_new_customer'),
        path('staff_loan_applications/', views.staff_loan_applications, name='staff_loan_applications'),
        path('staff_approve_loan/<int:pk>/<slug:user>', views.staff_approve_loan, name='staff_approve_loan'),
        path('staff_deny_loan/<int:pk>/<slug:user>', views.staff_deny_loan, name='staff_deny_loan'),

        path('logout/', views.logout, name='logout'),
]
