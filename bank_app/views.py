from decimal import Decimal
from secrets import token_urlsafe
from django.http import HttpResponseRedirect
from django.shortcuts import render, reverse, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.db import IntegrityError, transaction
from .forms import TransferForm, UserForm, CustomerForm, NewUserForm, NewAccountForm
from .models import Account, Ledger, Customer, LoanApplications, UID
from .errors import InsufficientFunds
import requests
from django.contrib.auth import logout as dj_logout


@login_required
def index(request):
    if request.user.is_verified():
        if request.user.is_staff:
            return HttpResponseRedirect(reverse('bank_app:frontpage_staff'))
        else:
            return HttpResponseRedirect(reverse('bank_app:frontpage_customer'))
    else:
        if request.user.is_staff:
            return HttpResponseRedirect(reverse('bank_app:frontpage_staff'))
        else:
            context = {}
            return render(request, 'two_factor/core/otp_required.html', context)


# Customer views

@login_required
def frontpage_customer(request):
    assert not request.user.is_staff, 'Staff user routing customer view.'

    accounts = request.user.customer.accounts
    context = {
        'accounts': accounts,
    }
    return render(request, 'bank_app/frontpage_customer.html', context)


@login_required
def account_details(request, pk):
    assert not request.user.is_staff, 'Staff user routing customer view.'

    account = get_object_or_404(Account, user=request.user, pk=pk)
    context = {
        'account': account,
    }
    return render(request, 'bank_app/account_details.html', context)


@login_required
def transaction_details(request, transaction):
    movements = Ledger.objects.filter(transaction=transaction)
    if not request.user.is_staff:
        if not movements.filter(account__in=request.user.customer.accounts):
            raise PermissionDenied('Customer is not part of the transaction.')
    context = {
        'movements': movements,
    }
    return render(request, 'bank_app/transaction_details.html', context)

@login_required
def transfer_options(request):
    context = {}
    return render(request, 'bank_app/transfer_options.html', context)

@login_required
def make_transfer(request):
    assert not request.user.is_staff, 'Staff user routing customer view.'

    if request.method == 'POST':
        form = TransferForm(request.POST)
        form.fields['debit_account'].queryset = request.user.customer.accounts
        if form.is_valid():
            amount = form.cleaned_data['amount']
            debit_account = Account.objects.get(pk=form.cleaned_data['debit_account'].pk)
            debit_text = form.cleaned_data['debit_text']
            credit_account = Account.objects.get(pk=form.cleaned_data['credit_account'])
            credit_text = form.cleaned_data['credit_text']
            try:
                transfer = Ledger.transfer(amount, debit_account, debit_text, credit_account, credit_text)
                return transaction_details(request, transfer)
            except InsufficientFunds:
                context = {
                    'title': 'Transfer Error',
                    'error': 'Insufficient funds for transfer.'
                }
                return render(request, 'bank_app/error.html', context)
    else:
        form = TransferForm()
    form.fields['debit_account'].queryset = request.user.customer.accounts
    context = {
        'form': form,
    }
    return render(request, 'bank_app/make_transfer.html', context)

@login_required
def make_external_transfer(request):
    if request.method == 'POST':
        amount = int(request.POST['amount'])
        credit_account = int(request.POST['credit_account'])
        debit_account = request.POST['debit_account']
        credit_text = request.POST['credit_text']
        debit_text = request.POST['debit_text']
        transfer_url = 'http://127.0.0.1:4000/external_transfer/transfer'
        id_data = {'id': credit_account}
        post_response = requests.post(transfer_url, data=id_data)
        if post_response.status_code == 200:
            transaction_id = UID.uid
            put_url = f'http://127.0.0.1:4000/external_transfer/confirm_transfer/{transaction_id}'
            put_data = {"amount": amount, "credit_account": credit_account, "credit_text": credit_text}
            put_response = requests.put(put_url, data=put_data)
            if put_response.status_code == 200:
                try:
                    d_account = get_object_or_404(Account, id=debit_account)
                    transfer = Ledger.objects.create(amount=-amount, transaction=transaction_id, account=d_account, text=debit_text)
                    return transaction_details(request, transaction_id)
                except InsufficientFunds:
                    delete_url = 'http://127.0.0.1:4000/external_transfer/rollback'
                    delete_response = requests.delete(delete_url, data={'id': transaction_id})
                    self.assertEquals(delete_response.status_code, 200)
                    context = {
                        'title': 'Transfer Error',
                        'error': 'Insufficient funds for transfer.'
                    }
                    return render(request, 'bank_app/error.html', context)
            else:
                context = {
                    'title': 'Transfer Error',
                    'error': f'Something went wrong.. error: {put_response.status_code}'
                }
                return render(request, 'bank_app/error.html', context)
        else:
            context = {
                'title': 'Transfer Error',
                'error': f'Something went wrong.. error: {post_response.status_code}'
            }
            return render(request, 'bank_app/error.html', context)
    
    form = TransferForm()
    form.fields['debit_account'].queryset = request.user.customer.accounts
    context = {
        'form': form,
    }
    return render(request, 'bank_app/make_external_transfer.html', context)

@login_required
def apply_loan(request):
    assert not request.user.is_staff, 'Staff user routing customer view.'

    if not request.user.customer.can_make_loan:
        context = {
            'title': 'Apply Loan Error',
            'error': 'Only Gold members can apply for loans.'
        }
        return render(request, 'bank_app/error.html', context)

    if request.method == 'POST':
        user = request.user
        name = request.POST['name']
        amount = Decimal(request.POST['amount'])
        loan_desc = request.POST['loan_desc']
        try:
            LoanApplications.objects.create(user=user, name=name, amount=amount, loan_desc=loan_desc)
            return HttpResponseRedirect(reverse('bank_app:frontpage_customer'))
        except ServerFail:
                context = {
                    'title': 'Loan Application Error',
                    'error': 'Server error'
                }
                return render(request, 'bank_app/error.html', context)
        
    return render(request, 'bank_app/apply_loan.html', context={})

# Staff views

@login_required
def frontpage_staff(request):
    assert request.user.is_staff, 'Customer user routing staff view.'

    return render(request, 'bank_app/frontpage_staff.html')


@login_required
def staff_search_partial(request):
    assert request.user.is_staff, 'Customer user routing staff view.'

    search_term = request.POST['search_term']
    customers = Customer.search(search_term)
    context = {
        'customers': customers,
    }
    return render(request, 'bank_app/staff_search_partial.html', context)


@login_required
def staff_customer_details(request, pk):
    assert request.user.is_staff, 'Customer user routing staff view.'

    customer = get_object_or_404(Customer, pk=pk)
    if request.method == 'GET':
        user_form = UserForm(instance=customer.user)
        customer_form = CustomerForm(instance=customer)
    elif request.method == 'POST':
        user_form = UserForm(request.POST, instance=customer.user)
        customer_form = CustomerForm(request.POST, instance=customer)
        if user_form.is_valid() and customer_form.is_valid():
            user_form.save()
            customer_form.save()
    new_account_form = NewAccountForm()
    context = {
        'customer': customer,
        'user_form': user_form,
        'customer_form': customer_form,
        'new_account_form': new_account_form,
    }
    return render(request, 'bank_app/staff_customer_details.html', context)


@login_required
def staff_account_list_partial(request, pk):
    assert request.user.is_staff, 'Customer user routing staff view.'

    customer = get_object_or_404(Customer, pk=pk)
    accounts = customer.accounts
    context = {
        'accounts': accounts,
    }
    return render(request, 'bank_app/staff_account_list_partial.html', context)


@login_required
def staff_account_details(request, pk):
    assert request.user.is_staff, 'Customer user routing staff view.'

    account = get_object_or_404(Account, pk=pk)
    context = {
        'account': account,
    }
    return render(request, 'bank_app/account_details.html', context)


@login_required
def staff_new_account_partial(request, user):
    assert request.user.is_staff, 'Customer user routing staff view.'

    if request.method == 'POST':
        new_account_form = NewAccountForm(request.POST)
        if new_account_form.is_valid():
            Account.objects.create(user=User.objects.get(pk=user), name=new_account_form.cleaned_data['name'])
    return HttpResponseRedirect(reverse('bank_app:staff_customer_details', args=(user,)))


@login_required
def staff_new_customer(request):
    assert request.user.is_staff, 'Customer user routing staff view.'

    if request.method == 'POST':
        new_user_form = NewUserForm(request.POST)
        customer_form = CustomerForm(request.POST)
        if new_user_form.is_valid() and customer_form.is_valid():
            username    = new_user_form.cleaned_data['username']
            first_name  = new_user_form.cleaned_data['first_name']
            last_name   = new_user_form.cleaned_data['last_name']
            email       = new_user_form.cleaned_data['email']
            password    = token_urlsafe(16)
            rank        = customer_form.cleaned_data['rank']
            personal_id = customer_form.cleaned_data['personal_id']
            phone       = customer_form.cleaned_data['phone']
            try:
                user = User.objects.create_user(
                        username=username,
                        password=password,
                        email=email,
                        first_name=first_name,
                        last_name=last_name
                )
                print(f'********** Username: {username} -- Password: {password}')
                Customer.objects.create(user=user, rank=rank, personal_id=personal_id, phone=phone)
                return staff_customer_details(request, user.pk)
            except IntegrityError:
                context = {
                    'title': 'Database Error',
                    'error': 'User could not be created.'
                }
                return render(request, 'bank_app/error.html', context)
    else:
        new_user_form = NewUserForm()
        customer_form = CustomerForm()
    context = {
        'new_user_form': new_user_form,
        'customer_form': customer_form,
    }
    return render(request, 'bank_app/staff_new_customer.html', context)


@login_required
def staff_loan_applications(request):
    loan_applications = LoanApplications.objects.filter(approved=False, denied=False)
    context = {
        'loan_applications': loan_applications,
    }
    return render(request, 'bank_app/staff_loan_applications.html', context)

@login_required
def staff_approve_loan(request, pk, user):
    if request.method == 'POST': 
        loan = LoanApplications.objects.filter(pk=pk)
        loan_user = User.objects.filter(username=user).first()
        amount = request.POST['amount'].replace(",", "")
        with transaction.atomic():
            loan.update(approved=True)
            loan_user.customer.make_loan(Decimal(amount), request.POST['name'])
        return HttpResponseRedirect(reverse('bank_app:staff_loan_applications'))
    else: 
        return render(request, 'bank_app/error.html', context={})

@login_required
def staff_deny_loan(request, pk, user):
    if request.method == 'POST': 
        loan = LoanApplications.objects.filter(pk=pk)
        loan.update(denied=True)
        return HttpResponseRedirect(reverse('bank_app:staff_loan_applications'))
    else: 
        return render(request, 'bank_app/error.html', context={})

def logout(request):
    dj_logout(request)
    return HttpResponseRedirect(reverse('two_factor:login'))
