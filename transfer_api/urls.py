from django.urls import path
from . import views

urlpatterns = [
        path('', views.test),
        path('transfer', views.transfer),
        path('uid_test', views.UID_test),
        path('get_ledger', views.get_ledger),
        path('confirm_transfer/<int:transfer_id>', views.confirm_transfer),
]