from rest_framework import serializers
from bank_app.models import Account, UID, Ledger

class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account 
        fields = '__all__'

class UIDSerializer(serializers.ModelSerializer):
    class Meta:
        model = UID 
        fields = '__all__'

class LedgerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ledger 
        fields = ('account', 'transaction', 'amount', 'text')