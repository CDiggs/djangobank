from rest_framework.response import Response
from rest_framework.decorators import api_view
from bank_app.models import Account, Ledger, UID
from .serializers import AccountSerializer, UIDSerializer, LedgerSerializer
from django.shortcuts import get_object_or_404
from django.db import transaction

@api_view(['GET'])
def test(request):
    accounts = Account.objects.all() 
    serializer = AccountSerializer(accounts, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def UID_test(request):
    UIDS = UID.objects.all() 
    serializer = UIDSerializer(UIDS, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def get_ledger(request):
    ledgers = Ledger.objects.all() 
    serializer = LedgerSerializer(ledgers, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def transfer(request):
    data = request.data
    aid = data['id']
    account = get_object_or_404(Account, id=aid)
    serializer = AccountSerializer(account, many=False)
    return Response(serializer.data)

@api_view(['PUT'])
def confirm_transfer(request, transfer_id):
    data = request.data
    amount = data['amount']
    credit_id = data['credit_account']
    credit_account = get_object_or_404(Account, id=credit_id)
    credit_text = data['credit_text']
    uid = UID.objects.create(id=transfer_id)
    transfer = Ledger.objects.create(amount=amount, transaction=uid, account=credit_account, text=credit_text)
    serializer = LedgerSerializer(transfer, many=False)
    return Response(serializer.data)

@api_view(['DELETE'])
def rollback(request):
    data = request.data
    trans_id = data['id']
    ledge = get_object_or_404(Leder, transaction=trans_id)
    serializer = LedgerSerializer(ledge, many=False)
    ledge.delete()
    return Response(serializer.data)
